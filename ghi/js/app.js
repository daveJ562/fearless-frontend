function createCard(name, description, pictureUrl, starts, ends, location) {
  return `
        <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
            <div class="card-footer">
                ${starts} - ${ends}
            </div>
        </div>
        </div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";
  const columns = document.querySelectorAll(".col");
  let colIndex = 0;

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        console.log(conference);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          // console.log(details);
          const name = details.conference.name;
          // console.log(name);
          const description = details.conference.description;
          // console.log(description);
          const pictureUrl = details.conference.location.picture_url;
          // console.log(pictureUrl);

          const conferenceStart = new Date(
            details.conference.starts
          ).toDateString();

          const conferenceEnd = new Date(
            details.conference.ends
          ).toDateString();

          const location = details.conference.location.name;
          console.log("test");

          const html = createCard(
            name,
            description,
            pictureUrl,
            conferenceStart,
            conferenceEnd,
            location
          );
          // console.log(html);
          const column = columns[colIndex % 3];
          column.innerHTML += html;
          colIndex = (colIndex + 1) % 3;
        }
      }
    }
  } catch (e) {
    alert(`${e} Could not fetch the url`, "uh oh");
    console.error("error", e);
    // Figure out what to do if an error is raised
  }
});

// const conference = data.conferences[0];
// const nameTag =document.querySelector('.card-title');
// nameTag.innerHTML = conference.name;

// const detailUrl = `http://localhost:8000${conference.href}`;
// const detailResponse = await fetch(detailUrl);
// if (detailResponse.ok) {
//     const details = await detailResponse.json();
//     console.log(details);

//     const description = details.conference.description
//     // console.log(description);
//     const descriptionTag = document.querySelector('.card-text');
//     descriptionTag.innerHTML = description;

//     const pictureUrl = details.conference.location.picture_url;
//     console.log(pictureUrl)
//     const imageTag = document.querySelector('.card-img-top')
//     imageTag.src = details.conference.location.picture_url;

// const response = await fetch(url);
// console.log(response);

// const data = await response.json();
// console.log(data);
